#!/bin/bash

SOCKET=${SOCKET:=/var/run/docker.sock}
PORT=${PORT:=2375}

main() {
    if [ -e $SOCKET ]; then
        socat -d -d TCP-L:${PORT},fork UNIX:${SOCKET}
    else
        cat <<EOF
    Please mount /var/run/docker.sock
EOF
    fi
}

main "$@"
