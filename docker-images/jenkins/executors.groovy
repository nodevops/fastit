import jenkins.model.*

def env = System.getenv()

def nbExecutors = env['J_NB_EXECUTORS'] ?: 2

Jenkins.instance.setNumExecutors(nbExecutors as int)