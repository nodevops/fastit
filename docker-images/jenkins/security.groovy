#!groovy

import jenkins.model.*
import hudson.security.*
import jenkins.security.s2m.AdminWhitelistRule

def instance = Jenkins.getInstance()
def env = System.getenv()

def hudsonRealm = new HudsonPrivateSecurityRealm(false)
def userName = env['J_USER_NAME'] ?: 'admin'
def userPassword = env['J_USER_PASSWORD'] ?: 'admin'


hudsonRealm.createAccount(userName, userPassword)
instance.setSecurityRealm(hudsonRealm)

def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
instance.setAuthorizationStrategy(strategy)
instance.save()

Jenkins.instance.getInjector().getInstance(AdminWhitelistRule.class).setMasterKillSwitch(false)
