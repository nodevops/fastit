#!/bin/bash

JENKINS_PLUGIN_FILE=${JENKINS_PLUGIN_FILE:-/etc/jenkins/plugins.txt}

while [ ! -f "${JENKINS_PLUGIN_FILE}" ]; do
  date
  echo "Sleeping while plugin file is ready"
  sleep 1
done

echo "First plugin download phase"
/usr/local/bin/install-plugins.sh <"${JENKINS_PLUGIN_FILE}"
echo
echo "Go go go!"
echo
exec /sbin/tini -- /usr/local/bin/jenkins.sh
