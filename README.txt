## UI

docker-machine create -d virtualbox --virtualbox-boot2docker-url ./rancheros.iso --virtualbox-memory 2048 rancher-ui

## node

docker-machine create -d virtualbox --virtualbox-boot2docker-url ./rancheros.iso --virtualbox-memory 4096 rancher-node-01

## TODO

cible:
- construire et déployer app springboot + angular
  - dans openshift
  - dans rancher

### stack à construire

- maven artifact repo
- docker registry

=> nexus3 qui fait les 2?
=> artifactory oss + autre chose?

- tester pipeline qui utilise des images docker pour builder maven/node
- tester pipeline jenkins qui pousse artifact maven dans nexus/artifactory
- tester pipeline jenkins qui génère une appli front



